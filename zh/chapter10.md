### X.

君士坦丁堡的外在特征与罗马一样，仍然完全是异教。出于政治原因，君士坦丁先是容忍基督教与旧宗教并存，后来又将基督教置于旧宗教之上，但这位迷信的皇帝本人却将基督教与异教混为一谈。他在新城市里建造了教堂，还建造了希腊神庙，比如在论坛上建造了瑞亚神庙，在提契翁（Tycheion）上树立了城市幸运女神的形象，还在赛马场附近建造了迪奥斯库里神庙。

一些古老的拜占庭圣殿，其中著名的有密涅瓦-埃克巴西亚神庙、波塞冬神庙、赫卡特神庙、普罗瑟皮娜神庙、宙斯神庙和阿波罗-宙西普斯神庙，都被改建成了教堂，而其他一些神庙，即雅典卫城上的赫利俄斯神庙、阿佛洛狄忒神庙和阿耳忒弥斯神庙，则幸免于难；只有狄奥多西大帝将它们摧毁或用于最亵渎、最卑劣的服务。 这位皇帝将一座磷神狄安娜神庙供奉给了神圣的 Photina。哈默，《君士坦丁堡和博斯普鲁斯海峡》，I, 189页。

没有什么比这位皇帝在新城设置的魔法护身符更能体现君士坦丁融合基督教与希腊信仰和崇拜形式的特点了。他广场上的斑岩石碑上有自己的像柱，但这是一个用伊利乌姆（Ilium）矿石制成的古老的阿波罗像，只是它的头是新的，是皇帝的肖像。石像周围环绕着由七根钉子组成的光环，据说救世主就是被这七根钉子钉在十字架上的。十字架本身的碎片嵌在阿波罗雕像的身上。

在宏伟的 Milium 建筑上可以看到同一位皇帝和他的母亲海伦娜的巨像；两人中间都有十字架，但君士坦丁堡神奇的 Tyche 或 Anthusa 被绑在这条象征基督教信仰的链子上。异教的幸运女神也站在广场上赫利奥斯的太阳战车上，头上有十字架的标志。

君士坦丁的镀金巨像上也有同样的幸运女神，根据其自身的目的，在献城日归来时（330 年 5 月 11 日），巨像被庄严地摆放在游行队伍中。这尊雕像在庄严的游行队伍中举着火把穿过河马广场，被带到皇帝的所在地--法庭前，皇帝必须在那里跪拜这尊雕像和这座城市的泰姬。另见 Lasaulx，《希腊化的衰落》，第 45 页。

博斯普鲁斯海峡上的这座城市被它的幸运女神注定要吸干周边国家的道德和物质力量；希腊的古老宗教和文化通过这座城市被摧毁，与希腊宗教密不可分的希腊古代艺术圣迹被当作战利品运走，用于城市的外部装饰。

为此，君士坦丁掠夺了帝国各省，甚至罗马城，但首先是希腊和希腊亚洲的无数艺术珍品。作为异教的狂热敌人，这位皇帝将希腊的杰作视为无主财产；如果他没有将它们与神庙本身一起摧毁，他就会将它们运到博斯普鲁斯海峡。

甚至他的孙子也把雅典的艺术品运到了首都。其中可能包括雅典卫城的一幅雅典肖像画。雅典阿瑞斯神庙的大象雕像被这位皇帝放在君士坦丁堡的奥拉门（Porta Aura）。Antiquit. 君士坦丁堡，载于 Banduri Imp. Orient，I.21。 这个门上装饰着许多雕像，包括狄奥多西大帝的雕像。他的孙子在新城墙完工后也在这里树立了自己的雕像，这座雕像可能与大象有关。不过，既然塞德雷努斯说它们代表狄奥多西二世进城时所用的那些，那么科迪努斯关于它们起源于雅典的说法就值得怀疑了。

这些从古代世界汇集而来的雕塑，即使不是在数量上，也肯定在理想价值上超越了当时罗马的艺术珍品。在雅典娜时代，君士坦丁堡已经是除罗马之外世界上最大的艺术博物馆。数以千计的古代雕像，包括保萨尼亚斯在游历希腊城市时欣赏过的不朽艺术家的著名作品，如今装饰着巨大的帝国广场、巴登浴场、门廊、剧院、马戏团、帝国城堡、元老院宫殿和贵族宫殿。

所有这些希腊天才的建筑都与希腊的历史、崇拜和光荣记忆割裂开来，被带到了古老的遗址、集市广场、市政厅和神庙中，而艺术家们曾经为这些遗址的建筑和景观条件进行过构思和设计。现在，它们成了征服世界的新宗教和拜占庭凯撒大帝的战利品，而凯撒大帝的道德力量则来自基督教。它们只是君士坦丁堡建筑的任意装饰，无论这些建筑多么宏伟壮丽，都与希腊人的美的理想不相称，因为它们属于君士坦丁衰落的时代。

当雅典娜伊斯看到广场和街道上随处可见的祖国古老的神灵和英雄时，她的希腊心灵一定是尴尬多于喜悦。她在皇宫里也遇到了他们，尤其是在圣杯的廊柱上，因为那里竖立着许多雕像，包括雅典人的雕像。君士坦丁先是把著名的赫利孔缪斯带到帝国城堡，后来又把它们带到帝国元老院的宫殿，404 年 6 月圣索菲亚教堂和宫殿被大火烧毁时，它们和宫殿中保存的许多其他杰作一起消失了。只有多多纳的宙斯和密涅瓦奇迹般地幸免于难。

当年轻的女皇在河马竞技场的法庭上就座时（她自己的雕像后来被放置在她的画像旁边），她看到在马戏团的脊柱上和有柱子的大厅里，在从雅典和希腊、从 Cyzicus、Rhodus 和 Caesarea、Thralles 和 Ephesus 以及其他城市运来的艺术品中，有德尔斐的毕底亚阿波罗的三脚架，这是希腊人在普拉提亚的胜利中献给世界的著名礼物。她在那里看到了莱西普斯的大力神、宙斯柱、戴安娜、德尔斐阿波罗和雅典娜。金鼎由一个盘蛇的矿石底座支撑。只有这个传到了君士坦丁堡。在盘蛇上还可以读出参加普拉提亚战役胜利的城市名称。蛇柱 "的其余部分仍矗立在阿特米丹。O. 弗里克，《Das platäische Weihgeschenk zu Constantinopel》（Jahrbücher für class. Philol. von Fleckstein, III. Supplementband）。Petri Gyllii Topogr. Constan.，第 83 页。

雅典娜看到了伟大的希腊诗人、哲学家和演说家，其中包括沉思的荷马（一件令人惊叹的艺术品）、萨福、柏拉图、亚里士多德、佩里克利斯和德摩斯梯尼，以及许多政治家和将军、希腊的神和英雄，这些都汇集在塞维鲁皇帝创建、君士坦丁大帝扩建的宏伟的宙西普斯浴场中。它可能是君士坦丁堡最丰富的艺术博物馆，描绘了古代神话和历史中的主要人物。它还收藏了特洛伊史诗中的所有英雄和女英雄，包括一座宏伟的海伦雕像，根据一位抒情诗人的说法，这座雕像仍然唤醒着奥雷对爱情的渴望： 'τω̃ναγαλμάτωντω̃νειςτὸδημόσιονγυμνάσιοντου̃επικαλουμένουΖευξίππου。另见海涅的论文：Priscae artis opera quae Cpoli extitisse memorantur，载于 Comment. Soc. R. Gotting. A. 1793, XI, 7 ff.

雅典娜伊斯以一个受过古典教育的雅典妇女的喜悦心情，欣赏了劳苏斯宫殿中希腊艺术的著名作品，这位在阿卡迪乌斯统治下闻名于世的贵族，在帝国城堡和君士坦丁广场之间建造了劳苏斯宫殿、明多斯的厄洛斯、利西普斯的萨米安赫拉、林多斯的密涅瓦、西利斯和迪波伊诺斯的作品，以及克尼多斯的普拉克西特列斯的阿佛洛狄忒。 塞德雷纳斯（Cedrenus）在第 I 卷第 564 页列举了那里的雕像。

据说菲迪亚斯的奥林匹亚宙斯雕像在 394 年被皇帝狄奥多西下令从他的神庙中拿走后，也被放置在同一座宫殿中。海恩对此表示怀疑。君士坦丁堡的一些古物被当作历史上著名的艺术品。393 年夏天，参加奥林匹克运动会的希腊人最后一次惊叹于这座高大的宙斯雕像，它是异教和所有造型艺术的理想之神。因为紧接着，狄奥多西一世就颁布了一项法令，永远禁止庆祝奥林匹克运动会。

尤多西亚可能亲眼目睹了奥林匹亚神庙的毁灭，宙斯雕像曾让希腊人激动了几个世纪。Lasaulx, 第 110 页，引用了 Scholiast 引用卢西安著作的相关段落。Cedrenus, I, 616, Zonaras, II, XIV, 41, Lasaulx, Hertzberg, III, 461, 但 Nicetas Choniata (Fabricius Bibl. Graec. VI. 405) 在 1204 年君士坦丁堡被弗肯兰人摧毁的艺术品中提到了海伦、Lysippus 的海格力斯、萨摩斯的赫拉和雅典娜的巨像。此后不久，在巴西利斯库斯篡位期间的人民起义中，君士坦丁堡的大图书馆也被大火烧毁。

雅典娜伊斯和她那个时代任何一个热爱家乡的希腊人，在看到聚集在皇城的希腊艺术品时，都无法安慰自己，认为它们在这里找到了避难所，在这里它们得到了保护，不会在外省被狂热的基督徒或野蛮人的破坏性怒火摧毁。拜占庭帝国时期，这些艺术博物馆屡遭地震破坏，马凯利努斯（Marcellinus）记录了 447 年的一次地震导致金牛座广场（Forum Tauri）的许多雕像倒塌。狄奥多西二世时期的火灾：433 年、448 年、450 年，其中 Troadic 门廊毁于一旦。海涅，第 XII 卷。De Interitu operum quae Cpli 等。532 年查士丁尼统治下的尼西亚起义期间，查尔克宫、奥古斯都宫、圣索菲亚教堂、元老院宫、宙西普斯巴登浴场和许多其他著名建筑化为灰烬，许多都毁于那场可怕的大火。

这些艺术珍品的珍贵遗迹后来被法兰克十字军摧毁。因此，君士坦丁城市的博物馆无法对艺术的进一步发展产生任何影响。它们被人类遗忘了。当古神和英雄们从罗马的废墟中重新崛起，使欧洲美术复兴成为可能时，古希腊的正统杰作早已消亡或被遗忘。
