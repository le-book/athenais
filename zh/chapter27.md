###二十七.

狄奥多西在克里萨菲乌斯（Chrysaphius）和尤提克派（Eutychians）的劝说下屈服于亚历山大派，但他对东方教会的征服并不能持久。强盗宗教会议之后，君士坦丁堡和亚洲的神职人员中立即出现了一种教会反应。伟大的教皇利奥（Leo）给予了支持，他谴责了基督一性一派的教义。

狄奥多西本人一向没有自己的意志，而且善变，他让自己相信宗主教弗拉维安受到了不公正的对待。他把他的妹妹普尔切里亚从流放了一年的希伯蒙召回。他剥夺了狡诈的克里萨菲乌斯的职务，没收了他的财产，并将他流放到一个小岛上，正如尼斯福鲁斯（Nicephorus）第十四章第 49 节所述。

不久之后，皇帝还没来得及看清教堂里仍在肆虐的骚乱，就被死神吓了一跳。他不幸从马上跌入利库斯河，脊椎受伤，两天后于 450 年 7 月 28 日去世，享年 50 岁。他仍然抽出时间把他的妹妹普尔切里亚、帕特里西乌斯-阿斯帕、元老院首脑和帝国的其他大人物召集在一起，向他们表达了自己希望有一位帝国王位继承人的愿望。他向他们透露，有一天他在教堂里问神学家圣约翰谁应该继承他的帝位，梦中有人告诉他马尔西亚努斯将是他的继承人。这位垂死皇帝的忏悔证明，即使在基督教的最高层，异教的梦兆仍然存在。

狄奥多西大帝的孙子被庄严地安葬在使徒大教堂中他父亲和祖父的纪念碑下。他带着一个偏执、善良和仁慈的王子的名声入土为安，不是因为他的恶习，而是因为他的意志薄弱，使他成为宦官们的玩物。这些吸血虫的贪婪和匈奴和平的可耻决定了苏伊达斯对这位皇帝的严厉评价，他实际上称其为懦夫，却没有考虑到这位孙子比他的父亲和祖父更可爱的美德。

帝国的继承权本应传给狄奥多西的独生女，但她是罗马皇帝的妃子。她和皇帝都没有要求继承东方的王位；皇后的遗孀尤多西亚（Eudocia）也没有要求继承王位，因为她当时也流亡在遥远的耶路撒冷。奥古斯塔-普尔切里亚被帝国元老院宣布为她哥哥的继承人。

由一位女性独揽大权，这在罗马帝国的历史上是前所未有的。她认识到了这一点，并向寡居的马尔西亚努斯提出了柏拉图式的婚姻。

这位值得尊敬的 Patrician 是一位勇敢而聪明的男子，现年 54 岁，是色雷斯一个普通战士的儿子。他曾以卑微的身份来到君士坦丁堡，在阿达布里乌斯和阿斯帕手下服役，并在波斯战争和汪达尔战争中表现出色。在不幸的非洲战役中，他被根舍里奇俘虏，然后被移交给了他。据传说，汪达尔国王看到一只伸展着翅膀的雄鹰在他无忧无虑沉睡的俘虏头上盘旋，并意识到马尔西安前途无量。

普尔切丽娅的选择没有遭到反对。450 年 8 月 25 日，在宗主教阿纳托利乌斯（Anatolius）的见证下，她的模拟形象在希伯多姆（Hebdomon）被拥戴为皇帝，亲手将王冠戴在了自己的头上。

普尔切丽亚的第一件事就是惩罚克里萨菲乌斯。他在梅兰提亚城门杀死了斯巴塔。马凯利努斯。Victor Tunnunensis 编辑。Cedrenus 知道他被引渡到了约旦。

新统治者的第二项行动是恢复宗主教弗拉维亚努斯的名誉。他的遗体和约翰-金口一样，被庄严地抬到首都，安葬在使徒穹顶。 Pulcheria 向教皇利奥报告了此事。Epist. Leonis, n. LXXVII.
