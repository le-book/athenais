### V.

狄奥多西二世是阿卡迪乌斯的小儿子，也是狄奥多西大帝的孙子，狄奥多西大帝在 395 年去世时将罗马帝国分为东西两半，由他的两个儿子分别继承，他坐上了君士坦丁的宝座。

年轻的狄奥多西于 401 年 4 月 10 日出生在君士坦丁堡，由该城著名的宗主教约翰-金口施洗。他在三岁多时失去了母亲尤多西亚。她是鲍托的女儿，鲍托是一位英勇的法兰克指挥官，在帝国服役期间声名显赫，并于 385 年成为执政官。

在浪漫的环境下，尤多西亚获得了皇后的头衔。叱咤风云的大臣鲁菲努斯满怀信心地期待着实现他的野心，即把自己的女儿嫁给年轻的皇帝阿卡迪乌斯；但他狡猾的对手尤特罗皮乌斯（Eutropius）对尤多西亚非凡魅力的描述和她的画像极大地刺激了王子的想象力，以至于他同意了最奇怪的政变。一支庄严的廷臣队伍从皇宫出发，带着珍贵的聘礼穿过首都的大街小巷，如人们所期待的那样，从鲁菲努斯（Rufinus）的家中迎接新娘。395 年 4 月 27 日，她在这里嫁给了年轻的阿卡迪乌斯。

这一事件的后果是鲁菲努斯的倒台和美丽、贪婪的尤多西亚的无限制统治。她树立了拜占庭妇女政权的第一个榜样，这个政权建立在宫廷宠妃、宦官和司铎的阴谋和贪得无厌的欲望之上。塞德里努斯（Cedrenus，I，585 年）称她为可支配的βάρβαρος γυνὴ καὶ θρασυκάρδιος。她因触犯了虚荣心而将约翰-金口主教（Bishop John Chrysostom）流放至死。当人们在圣索菲亚教堂附近的帝国元老院宫殿前为她竖立银质雕像时，人们用如此热烈的异教狂欢来庆祝这一节日，愤怒的金口在布道中公开斥责这位女皇，甚至大胆地将她描述为新的希罗底。这导致他被推翻。宗主教被废黜，流放到西里西亚；他在流放地与流放地之间徘徊，于 407 年 9 月 14 日在悲惨中死去。

在他被推翻几个月后，尤多西亚（Eudoxia）皇后于 404 年 10 月 6 日去世，她为完全不称职的妃子阿卡迪乌斯（Arcadius）生了一儿四女： Flaccilla，397 年 5 月 18 日出生；Aelia Pulcheria，399 年 1 月 19 日出生；Arcadia，400 年 4 月 3 日出生；狄奥多西二世，401 年 4 月 10 日出生；Marina，403 年 2 月 10 日出生。Aug.Byzantinae。

狄奥多西二世于 408 年 5 月 1 日继承了父亲的王位，六年前，他还在襁褓中就被宣布为奥古斯都，并在希伯蒙加冕。他作为一个孤儿，在巨大的压力下统治了半个罗马帝国。他完全没有自卫能力。他的叔叔，罗马软弱无能、毫无斗志的皇帝霍诺留（Honorius）决定亲自前往君士坦丁堡，或者至少任命忠诚的人作为他侄子在君士坦丁堡的监护人；然而，西方的动乱阻止了他这样做。

拜占庭历史学家曾用一个令人费解的事实来描述和解释东罗马帝国的绝境，以及少年狄奥多西奇迹般地保住了父亲的王位。据说阿卡迪乌斯在遗嘱中将儿子的监护权委托给了罗马人所有敌人中最具骑士精神的波斯国王伊兹德盖德。后者是基督徒的公开朋友，于是从自己的宫廷中派了一个聪明人安提阿哥斯到君士坦丁堡担任皇室遗孤的监护人，他在那里非常谨慎地完成了四年的使命，然后返回了克特西丰。Theophanes, Chronogr ed., Bonn, I, 126; Cedrenus, I, 586; Zonaras, Annal., III, 122; Nicephorus, II, c. 1; see Sievers, Studien zur Gesch.

如果这一事实真的是历史事实，那么没有什么能比它更清楚地说明罗马帝国两半时期国家的严重衰败了。拜占庭人将其如实相告，不加任何评论，而且还深信不疑。如果日耳曼冒险家--苏维人和汪达尔人--能够在西方篡夺罗马政府，那么也可以认为波斯臣子有可能在半亚述的拜占庭摄政，只要他信奉基督教，而安提阿哥这个名字就表明了此人的非波斯国籍。不过，也有可能这位波斯监护人是同名皇帝阿卡迪乌斯（Arcadius）的内侍总管，他将自己的儿子教育成了一名教育家，并在宫中长期掌权；马拉拉斯（Malalas），XIV，361，明确提到他是一名教育家。 另见苏格拉底，VII，1；提奥法尼斯，I，148；尼瑟弗鲁斯，II，c.1。

拉帕茨（Lettres de Syn.，第 372 页）从 Synesius 的第 22 封信中草率地得出结论，信中提到的 Anastasius 被 Arcadius 任命为其子女的监护人。然而，这只是此人自己子女的合法化。在君士坦丁堡的皇室家族中，并没有他可以委托的著名亲戚。这些孤儿在陌生人、国家和宫廷官员、宫女和宦官中间长大，他们禁欲的习惯证明，他们在宫廷礼仪的压力和宫廷神职人员的影响下度过了一个没有欢乐的童年。

最重要的是，皇城的宗主教将监督他们接受最初的教育。他是 406 年出生的阿提库斯，博学而虔诚，夜以继日地研读圣书，被尊为当时最伟大的演说家之一。 基督教司铎认为，上帝亲自为年轻的狄奥多西提供了明智而虔诚的教育，因此也保护了帝国免遭叛乱和篡位者的侵扰。

想象一下这些帝国儿童在君士坦丁巨大的宫殿里的生活也不无吸引力，多年来，一个男孩在他年幼的姐妹们的簇拥下，将自己的名字和权力赋予一个伟大帝国的国家行为和法律，并接受他的人民的敬意；因为他们在他身上尊崇罗马帝国的威严。没有一个省起兵造反，拜占庭娴熟的管理机制也没有失灵。

幸运的是，安特米乌斯自 405 年以来一直担任禁卫军长官，作为政治家，他拥有卓越的品质，他的一位同时代人称他为当时所有在世者中最有洞察力的人，他以首席大臣的身份管理帝国事务。

410 年，当意大利被野蛮人肆虐，罗马成为西哥特人的猎物，远至赫拉克勒斯之柱的西方省份落入日耳曼征服者之手时，高贵的安特米乌斯却能将匈奴大军赶过多瑙河，维护拜占庭帝国的和平。413 年，他在博斯普鲁斯海峡的陆地一侧筑起了坚固的城墙，使首都免受敌人的攻击。这些城墙长 14 毫米。这些城墙被称为新城墙或狄奥多西城墙。

安泰米乌斯的孙子曾一度在罗马登上凯撒的宝座，但从 415 年开始，他就从历史上消失了。他的继任者是西奥多西大帝时期的奥雷利安努斯（Aurelianus），他是哥特人盖纳斯的著名对手。狄奥多西大帝在 415 年 3 月 5 日给他的第一份敕书中称他为 "Praef. Praet. 日期为 415 年 3 月 5 日；后来就没有再给他的敕书了。然而，国家的管理权却落到了普尔切里亚公主的手中。

414 年 7 月 4 日，狄奥多西任命他 15 岁的妹妹为奥古斯塔。作为皇室的最高荣誉，他赋予了她共同执政的权利，而她的姐妹们只有 Nobilissima 或 Basilissa 的头衔。

拜占庭帝国的事务由一个年轻的女孩管理，这种闻所未闻的事实至少说明了普尔切丽亚的罕见品质。她对皇宫和国家的统治远远超过了她弟弟的青春年华。与她同时代的教会历史学家索佐梅努斯（Sozomenus）在赞美她的美德时说，上帝任命这个女孩作为她哥哥的共同管理者和监护人，目的是让他成为所有皇帝中最虔诚的一个。苏格拉底，IX, 1。 καὶ θει̃ον έλαβε νου̃ν. Theophan, I, 126.

贤淑睿智的希帕蒂娅残酷地牺牲在亚历山大的基督徒暴徒手中，使她成为衰落的异教的圣人，最后的晚霞照亮了她美丽的身影；雅典娜伊斯是一个过渡性的人物，是异教的叛徒；但普尔切丽亚却以她所有的纤维扎根于正统的基督教。她是那个时代教会中的个人力量。
