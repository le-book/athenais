### XVI.

431 年，尤多西亚失去了她的第二个女儿弗拉琪拉，马凯利努斯记录了弗拉琪拉的死亡，他称弗拉琪拉是狄奥多西的女儿。君士坦丁堡爆发了饥荒；当皇帝游行到粮仓时，人们甚至向他投掷石块以追赶他。他的军队在非洲也很不顺利；他派出的军队在拜占庭最优秀的将领阿斯帕尔的指挥下援助埃提乌斯领导的罗马人，结果被汪达尔国王击败。

关于拜占庭宫廷的情况就不多说了。许多年过去了，我们仍然看不到尤多西亚的身影。但当她将独生女嫁给罗马皇帝时，她的幸福之星再次闪耀。

自 425 年以来，拉文纳和君士坦丁堡两个宫廷之间的关系空前活跃。狄奥多西曾多次与他未来的丈夫瓦伦提尼安一起担任执政官。这位年轻的王子在妻妾成群的朝臣中长大，而他的母亲则是宫廷阴谋中的一颗棋子，统治着摇摇欲坠的罗马帝国。

加拉-普拉西迪亚在所有的公共和家庭关系中都遭遇不幸。在经历了这么多悲惨的命运之后，她唯一的女儿又给她带来了致命的痛苦。年幼的霍诺丽亚（Justa Grata Honoria）任由宫廷元帅欧根纽斯（Eugenius）引诱；她的母亲将她送往君士坦丁堡（434 年）以掩盖她的羞耻，而严厉的处女普尔切丽娅（Pulcheria）则将她关进了修道院。

当她的哥哥瓦伦提尼安（Valentinian）皇帝带着他的未婚妻尤多西亚（Eudoxia）来到君士坦丁堡时，尤多西亚已经在这个监狱里生活了三年。婚礼原定在帖撒罗尼迦举行，但年轻的王子出于骑士精神和爱火，匆匆赶往皇城拥抱岳父母和新娘。她异常美丽，关于她的传说一直流传到普罗科皮乌斯时代。

雅典娜伊斯的女儿已经十四五岁了，而她的丈夫瓦伦提尼安却已经十八岁了。437 年 10 月 29 日，这对夫妇接受了宗主教普罗克洛斯的祝福，随后举行了盛大的庆祝活动。 普罗克洛斯自 434 年起担任君士坦丁堡主教。 这场婚姻为东罗马帝国带来了西伊利里亚作为奖品，但丈夫不得不将其让给岳父。新婚夫妇前往帖撒罗尼迦过冬，第二年春天返回拉文纳。年轻的罗马皇后面临着艰难的命运；她的一生不乏不寻常的事件，但却不如她的母亲幸福。

尤多西亚现在达到了自己幸福的顶峰：她的女儿登上了罗马凯撒的宝座，而她自己则是拜占庭的女皇。然而，她的这种满足是以与唯一的孩子分离为代价的，她再也没有见过自己的孩子。从那时起，她就在宫中孤独地生活着，而她在宫中的地位，尤其是她与狄奥多西和普尔切里亚的关系，完全失去了一个皇女本该给她带来的活力。

她试图通过去耶路撒冷朝圣来减轻与女儿分离的痛苦，这次朝圣也许与其说是她自己的愿望，不如说是皇帝的愿望，皇帝曾对天发誓要送他的少女去圣城，以便在基督的坟墓前感谢上帝赐予他女儿幸福的婚姻和其他恩惠。

据说，欧多西亚本人也是在一位著名的圣人--年轻的梅拉纳的劝说下做出这一决定的。这位罗马妇女出身于高贵的元老院，曾在十四岁时不得不将自己的手交给一位高贵的青年阿庇尼阿努斯，但她使自己的丈夫改过自新，献身于上帝。他们把在拉丁姆的产业分给穷人，然后周游世界，到过西西里岛，到过迦太基，到过被放弃者的天堂--神秘的埃及，梅拉娜在那里建立了几所修道院。后来，她住进了耶路撒冷的救世主墓。

此时，她的叔叔、罗马市市长沃鲁西安努斯作为普拉西迪亚和瓦伦提尼安的使者来到君士坦丁堡，为小皇帝即将与尤多西亚公主举行的婚礼做安排。沃卢西恩希望再次见到他心爱的侄女梅拉娜；梅拉娜接受了他的邀请，希望能让仍是异教徒的沃卢西恩皈依基督教。

她从耶路撒冷来到君士坦丁堡，在劳苏斯宫受到接待。她发现自己的叔叔病得奄奄一息，在宗主教普罗克洛斯的雄辩支持下，她的劝说更容易使这位高贵的罗马人皈依基督教。沃鲁西亚努斯死后成为了一名基督徒。

圣人发现皇城仍然深受景教异端邪说的困扰，她本人也以虔诚的热情与之斗争。她还说服狄奥多西禁止聂斯脱里的书籍，狄奥多西在 435 年 7 月 30 日颁布了一项针对聂斯脱里派的法令。 她恳求皇帝和皇后放弃一切世俗的虚荣，正是她最终唤醒了欧多西亚前往应许之地的愿望。梅拉纳回到那里，在基督的坟墓前庆祝复活节，并完成了在各各他建造一座讲堂的工程。- Baronius ad a. 434 Tillemont, Mem. Eccl., XIV, 252, 将梅拉纳从君士坦丁堡返回的时间定在 438 年复活节之前。

在女皇前往巴勒斯坦之前，她目睹了一个激动人心的场面。438 年 1 月 23 日，三十年前在流亡中去世的约翰-金口的遗物从庞图斯的科马纳被运到君士坦丁堡，并被庄严地安葬在这里的使徒教堂。438 年，君士坦丁及其帝国继承人的陵墓以及拜占庭宗主教的陵墓都被安葬在这里。

狄奥多西通过对一位名人迟来的荣誉拯救，为自己的母亲尤多西亚（Eudoxia）（金口玉言曾是她的牺牲品）赎罪，而他自己并不觉得自己将另一位宗主教暴露在敌人面前。毫无疑问，这一虔诚的举动也与他女儿的遗赠有关，这增强了他的宗教情感。

狄奥多西更有理由感谢上帝和他的圣人，因为正是在这个时候，自君士坦丁以来颁布的帝国法律大全集才最终定稿，他将此委托给了一个由优秀的法律专家组成的委员会，该委员会由执政官兼前省长安提阿哥斯领导。举世闻名的《狄奥多西法典》于 438 年在君士坦丁堡出版，几年后瓦伦提尼安三世承认它是罗马帝国的通用法典。它使拜占庭帝国在罗马灭亡后得以幸存，使拜占庭帝国具有了坚实的公民凝聚力；它甚至向野蛮民族灌输了法律和文明的精神，并使软弱的狄奥多西获得了其臣民的伏尔泰的美誉。
