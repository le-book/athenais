### XXIV.

In das Leben des unglücklichen Theodosius hatten die finstern Mächte eingegriffen. Nachdem er dem Dämon der Eifersucht seine Gemalin und seinen Jugendfreund aufgeopfert und sich selbst mit Schuld belastet hatte, beraubte er sich auch eines seiner trefflichsten Staatsmänner.

Mit der Verbannung Eudokias muß der Sturz des Cyrus zusammenhängen, welcher wahrscheinlich bald nach der Ermordung des Saturninus erfolgt ist.

Der Consular und Patricius Cyrus war Präfect der Stadt, und bekleidete dies einflußreiche Amt vier Jahre lang.Malalas, XIV, 361. Es verlieh ihm die Aufsicht über die öffentlichen Bauwerke Constantinopels, für die er mit leidenschaftlichem Eifer Sorge trug. Ein Erdbeben hatte einen Teil der Stadtmauern niedergeworfen: er stellte sie mit großer Schnelligkeit wieder her.Nach Zonaras (II, 34) in 60 Tagen, was recht eilig ist. Cyrus muß viele Verdienste um die Hauptstadt gehabt haben, in welcher er unter anderm auch die öffentliche Beleuchtung der Werkstätten eingeführt hatte; denn eines Tags begrüßte ihn das Volk im Hippodrom, in Gegenwart des Kaisers, mit dem begeisterten Zuruf: »Constantin hat die Stadt erbaut, aber Cyrus hat sie erneuert.«

Der Stadtpräfect hatte Ursache über diesen tactlosen und für einen Staatsbeamten gefährlichen Beifall zu erschrecken. Seinen ahnungsvollen Ausruf: »Zu viel Lächeln des Glücks bringt Verderben«, machte der beleidigte Kaiser sofort zur Wahrheit.Ουκ αρέσκει μοι τύχη πολλὰ γελω̃σα. Malalas. Er ließ dem Lieblinge des Volks den Proceß machen, als sei er ein Anhänger des Heidentums.επλάκη ως ‛Ὲλλην: Malalas. διαβάλλεται ως ελληνόφρων: Cedrenus, I, 599. Als ein launischer Tyrann nahm er ihm Ehren, Würden und Vermögen.

Der unglückliche Cyrus flüchtete in das Asyl einer Kirche, aber dort ließ ihn der Kaiser festnehmen. Er zwang ihm das geistliche Gewand auf, wie er ein solches zuvor auch seinem Großkämmerer Antiochus angezogen hatte. Dieser reiche Patricius, einst der Erzieher des jungen Kaisers, war nochmals in Ungnade gefallen und zum Presbyter in Constantinopel gemacht worden.Das Jahr ist ungewiß. Von seinem Sturz: Malalas, XIV, 361. Theophanes, I, 148. Codinus, De aedif., S. 94, der von seinem Palast redet.

Es ist zweifelhaft, ob die damals überall im römischen Reich gewöhnliche Maßregel der Herrscher, ihnen mißliebig gewordene Laien, zumal von hoher Stellung, ohne Weiteres in den Priesterrock, bisweilen sogar einen bischöflichen, zu stecken, mehr für die Achtung als die Mißachtung des Standes der Cleriker spricht. Die geistliche Würde war für Verfolgte ein wirkliches Asyl und zugleich eine Strafe für Verurteilte.

Cyrus, welcher vielleicht noch einige Sympathie für die alten Götter Griechenlands empfand, und sicherlich sehr wenig Kenntnisse von den Pflichten eines Geistlichen am Altar einer Kirche hatte, wurde auf kaiserlichen Befehl mit kurzem Proceß zum Bischof von Cotyäum in Phrygien gemacht, vielleicht deshalb, weil die dortige Christengemeinde in dem Rufe stand, schon vier ihrer Bischöfe umgebracht zu haben.

Ihr neuer Seelsorger wider Willen stellte sich seiner räudigen Heerde zum Weihnachtsfest in der Kirche vor, und das dort versammelte Volk belustigte sich, den mutmaßlichen Heiden im Priestergewande zu einer Erbauungspredigt aufzufordern. Der geistvolle Expräfect zog sich mit Geschick aus dieser Verlegenheit: er bestieg die Kanzel und hielt folgende Rede: »Brüder, es geziemt sich, das Geburtsfest unsers Gottes und Heilandes Jesu Christi mit Schweigen zu feiern, denn durch das Gehör allein ist von der heiligen Jungfrau der Logos empfangen worden. Ihm sei die Ehre in Ewigkeit, Amen.«

Unter stürmischem Beifallsruf stieg der Redner von der Kanzel herab, und bis an seinen Tod ist Cyrus Bischof jener Stadt geblieben.Malalas. Zonaras, II, 34. Theophanes, I, 148. Beide nennen als Bistum des Cyrus Smyrna, Suidas Cotyäum, und dieser Lexikograph setzt seinen Sturz in die Zeit, da Eudokia (zum zweiten Male) in Jerusalem war. Sein Sohn soll der Dichter Paulus Silentiarius gewesen sein.

Eine auserlesene classische Bildung hatte ihn der Kaiserin Eudokia besonders wert gemacht. Am Hofe Constantinopels vertrat er die griechische Partei. Man beschuldigte ihn deshalb, ein Heide zu sein. Seine hellenischen Neigungen beleidigten, wie wol auch jene der Athenerin Eudokia selbst, die Grundsätze aller derjenigen Staatsmänner, die am officiellen Römertum festhielten.

Sie wollten nichts von dem griechischen Wesen wissen, sondern das byzantinische Reich sollte das lateinische Gepräge Constantins in der amtlichen Sprache wie in allen Einrichtungen des Staates für immer bewahren. Römer, nicht etwa Griechen zu sein, war der Stolz der Byzantiner. Auf ihren Münzen wurde nicht der Genius der Kaiserstadt Constantinopolis, sondern die altehrwürdige Roma abgebildet. Sie blickten schon mit Verachtung auf das Heidenland Hellas, und selbst noch in den Zeiten, wo die Sprache des Demosthenes und Thucydides die allgemeine des östlichen Reiches geworden war, nannten sie dieses das Römerreich, und sich selbst Romäer.

Noch im sechsten Jahrhundert hat Johannes Lydus, ein byzantinischer Autor, das Andenken des Cyrus als eines Feindes des Römertums gebrandmarkt. Der Römer Fontejus, so behauptete er, habe ein dem Romulus erteiltes altes Orakel bekannt gemacht, welches verkündete: das Glück werde die Römer nicht verlassen, so lange sie selbst nicht ihre Sprache vergäßen. Diese Weissagung sei wahr geworden, denn ein gewisser Cyrus, welcher nichts mehr als ein Poet gewesen, und dessen Verse noch heute bewundert würden, habe als Präfect der Stadt und des Prätoriums es gewagt, von der alten Gewohnheit abzuweichen und Decrete in griechischer Sprache zu erlassen. Mit der römischen Sprache sei aber auch das Ansehen des Amtes der Präfectur geschwunden, denn der Kaiser Theodosius habe, von Cyrus dazu überredet, dieser Magistratur die Macht entzogen.Johannes Lydus de Magistratibus ed. Bonn., lib. II, 13, p. 178; III, 42, p. 235.

Cyrus wurde also noch lange nach seinem Tode als Poet gefeiert; der Kirchengeschichtschreiber Evagrius nannte ihn noch am Ende des sechsten Jahrhunderts mit besonderer Auszeichnung als Dichter allein neben dem berühmten Claudianus.Evagrius, I, c. 19.

Wir können sein Dichtertalent nicht mehr hinreichend beurteilen, denn von seinen Poesien haben sich nur sechs Epigramme erhalten, welche die griechische Anthologie aufbewahrt. Sie bewegen sich durchaus in antiken Formen und Anschauungen.Anthol. Graeca er. Jacobs, VII, 557; IX, 136, 623, 808, 809; XV, 9, oder in der pariser Ausgabe von 1872: Epigrammatum Anthologia Palatina ed. Dübner.

In den Tagen seines Glückes am Hofe hatte Cyrus die Tugenden des Kaisers in derselben Höflingssprache verherrlicht, deren sich auch Sozomenus in Prosa bedient hat. Dies sind seine Verse auf den mittelmäßigen Theodosius:

> Alle die Werke Achills, die gepriesenen, sind dir zu eigen,  
Aber du bliebst vom lauernden Eros verschont. Von dem Bogen  
Schnellst du den Pfeil wie Teukros, doch du bist echteren Stammes;  
Agamemnon gleich an Gestalt, nur hat dir vom Weine  
Nie dein Geist sich erhitzt. An Verstande dem klugen Odysseus  
Aehnlich bist du, nur ohne die listigen Ränke. Dem greisen  
Pylier floß vom Mund nicht süßer die Rede, o Kaiser,  
Und doch lebtest du nicht, gleich ihm, drei Menschengeschlechter.Anthol., XV, n. 9.. ’Εγκώμιον εις Θεοδόσιον τὸν βασιλέα.

Diese schamlosen Verse waren freilich nicht homerisch genug, um den Präfecten Cyrus vor dem Sturze zu bewahren.

Als der Dichter aus der großen Kaiserstadt in das Exil gehen mußte, schrieb er folgende Seufzer nieder:

> 

So war auch der fromme »Kalligraph« Theodosius zu einem mißtrauischen Herrscher geworden: er hatte gelernt Günstlinge zu erheben und zu stürzen. Aber auch für ihn würden sich menschliche Milderungsgründe auffinden lassen, und zwar in dem Chaos der Leidenschaften und Cabalen des byzantinischen Palasts.

Dort vereinsamte der Kaiser immer mehr. Nachdem er sein Liebesglück zerstört hatte, geriet er unter die unwürdige Herrschaft des Spathars Chrysaphius, welcher sogar die Augusta Pulcheria aus ihrer Stellung zu verdrängen wußte.

Die beiden andern Schwestern des Theodosius starben, Arcadia im Jahre 444 und Marina 449, beide unvermält. Die Monumente ihres stillen Lebens waren Hospitäler und Kirchen, aber auch Paläste. Unter den sechs »Palästen der Kaiserinnen«, welche man in Constantinopel zählte, gab es zwei der Pulcheria, und je einen der Arcadia und Marina, während der Kaiserin Eudokia keiner zugeschrieben wird.Außer den genannten domus Augustarum zeigte man den Palast der Placidia Augusta und den der Eudoxia Augusta, der Mutter Theodosius II. Ducange, Const. christiana, lib. II, 141 fg.
